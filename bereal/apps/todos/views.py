from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from .models import Collection

def index(request):
    return render(request, "todos/index.html", {})

@login_required
def my_collections(request):
    mycollections = Collection.objects.filter(user=request.user)
    return render(request, "todos/my_collections.html", {"mycollections": mycollections})

