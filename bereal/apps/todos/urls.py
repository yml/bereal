from django.conf.urls.defaults import patterns, url, include

from .views import index, my_collections

urlpatterns = patterns('',
    url(r'^$', index, name="todos_index"),
    url(r'^todos/$', my_collections, name="todos_mycollections"),
)

