from django.db import models


class Collection(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey("auth.User")

    def __unicode__(self):
        return u"{0} for {1}".format(self.name, self.user)

class Item(models.Model):
    parent = models.ForeignKey(Collection)
    name = models.CharField(max_length=255)
    is_done = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name
