from django.conf.urls.defaults import patterns, url, include

from .views import ping, authenticated_ping

urlpatterns = patterns('',
    url(r'^ping/$', ping, name="core_ping"),
    url(r'^auth_ping/$', authenticated_ping, name="core_ping"),
)
