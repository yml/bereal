import json

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


def ping(request):
    return HttpResponse("pong")


@login_required
def authenticated_ping(request):
    user_id = request.user.id
    return HttpResponse(json.dumps({"user_id": user_id,
                                    "channels": ["chan_all",
                                                 "chan_user_%s" % user_id]}))
