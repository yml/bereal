.. 

bereal
======================

Quickstart
----------

To bootstrap the project::

    virtualenv bereal
    source bereal/bin/activate
    cd path/to/bereal/repository
    pip install -r requirements.pip
    pip install -e .
    cp bereal/settings/local.py.example bereal/settings/local.py
    manage.py syncdb --migrate

Documentation
-------------

Developer documentation is available in Sphinx format in the docs directory.

Initial installation instructions (including how to build the documentation as
HTML) can be found in docs/install.rst.
